package core.common;

public enum EnvironmentProperty {

    SQLITE_CONNECTION_STRING,
    BASE_SITE_URL;

    public String readProperty() {
        final String propertyName = getPropertyName();
        return PropertiesController.getProperty(propertyName);
    }

    public String getPropertyName() {
        return name().toLowerCase().replaceAll("_", ".");
    }
}
