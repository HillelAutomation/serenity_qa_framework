package core.common;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PropertiesController {

    private static PropertiesController instance;
    private final Properties properties = new Properties();
    private static final String PROPERTY_FILE_DEFAULT_PATH = "src/main/resources/environment/properties/";
    private static final String ENV_CONFIG_FILE = "env.config.file";
    private static final String PROPERTY_TEMPLATE = "\\$\\{([a-zA-Z0-9]+(?:\\.[a-zA-Z0-9]+)*+)}";

    private PropertiesController() {
        final String envConfigFile = System.getProperty(ENV_CONFIG_FILE);
        final String path = PROPERTY_FILE_DEFAULT_PATH + envConfigFile;
        mergeProperties(path);
        properties.put(ENV_CONFIG_FILE, path);
    }

    private static PropertiesController getInstance() {
        return Objects.isNull(instance) ? instance = new PropertiesController() : instance;
    }

    public static String getProperty(final String propertyName) {
        String propertyValue = System.getProperty(propertyName, getInstance().properties.getProperty(propertyName));
        if (Objects.nonNull(propertyValue) && Pattern.matches(PROPERTY_TEMPLATE, propertyValue)) {
            final Matcher m = Pattern.compile(PROPERTY_TEMPLATE).matcher(propertyValue);
            while (m.find()) {
                final String valueToReplace = m.group(1);
                propertyValue = StringUtils.replaceOnce(propertyValue, m.group(0), getProperty(valueToReplace));
            }
        }
        return propertyValue;
    }

    private void mergeProperties(final String path) {
        final File file = new File(path);
        final Properties envProperties = new Properties();
        try {
            final FileInputStream propFile = new FileInputStream(file);
            envProperties.load(propFile);
            for (final Object property : envProperties.keySet()) {
                final String key = (String) property;
                if (key.startsWith("+")) {
                    final String replacedKey = String.format("%s/%s", file.getParent(), StringUtils.trim(StringUtils.substring(key, 1)));
                    if (new File(replacedKey).exists()) {
                        mergeProperties(replacedKey);
                    } else {
                        System.out.println("Cannot find file!");
                    }
                }
            }
            properties.putAll(envProperties);
        } catch (final IOException e) {
            throw new IllegalStateException("Following property file is not found!", e);
        }
    }

}
