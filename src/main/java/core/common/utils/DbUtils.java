package core.common.utils;

import lombok.Data;

import java.sql.*;
import java.util.*;

@Data
public class DbUtils {

    private static final PropertyUtils PROPERTY_UTILS = new PropertyUtils();
    private static final String CONNECTION_STRING = PROPERTY_UTILS.getProperty("sqlite.connection.string");

    private static Connection connection;
    private static ResultSet resultSet;
    private static Statement statement;
    private static ResultSetMetaData resultSetMetaData;

    public DbUtils() {
        connection = openConnection();
        try {
            if (Objects.nonNull(connection) && !connection.isClosed()) {
                System.out.println("Connection has been established!");
            }
        } catch (final SQLException e) {
            throw new IllegalStateException("Unable to connect to db!", e);
        }
    }

    public Connection openConnection() {
        try {
            return DriverManager.getConnection(CONNECTION_STRING, "valer324_test", "valer324_test");
        } catch (final SQLException e) {
            throw new IllegalStateException("Unable to connect to db!", e);
        }
    }

    public static void main(String[] args) throws SQLException {
        DbUtils dbUtils = new DbUtils();
        //PreparedStatement statement = connection.prepareStatement("select * from valer324_test");
        statement = connection.createStatement();
        resultSet = statement.executeQuery("select * from test_remote");
        /* = resultSet.getMetaData();
        for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
            String columnName = resultSetMetaData.getColumnName(i);
            System.out.println(String.format("Column index [%s], name is : [%s]", i, columnName));
        }*/
        List<Map<String, String>> results = parseResult();
    }

    private static List<Map<String, String>> parseResult() throws SQLException {
        List<Map<String, String>> dbResult = new ArrayList<>();
        resultSetMetaData = resultSet.getMetaData();
        int columnCount = resultSetMetaData.getColumnCount();
        while (resultSet.next()) {
            for (int column_index = 1; column_index <= columnCount; column_index++) {
                Map<String, String> columnData = new HashMap<>();
                String columnName = resultSetMetaData.getColumnName(column_index);
                String columnValue = resultSet.getString(column_index);
                columnData.put(columnName, columnValue);
                dbResult.add(columnData);
            }
        }
        return dbResult;
    }
}
