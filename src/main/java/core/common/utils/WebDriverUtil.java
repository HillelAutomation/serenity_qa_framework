package core.common.utils;

import core.common.pages.AbstractPage;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;
import net.thucydides.core.webdriver.javascript.JavascriptExecutorFacade;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Optional;
import java.util.StringJoiner;

public class WebDriverUtil {

    private static String scriptToCheck;

    private WebDriverUtil() {
    }

    public static WebDriver getDriver() {
        return ThucydidesWebDriverSupport.getDriver();
    }

    public static <T extends AbstractPage> String getDefaultUrl(final Class<T> page) {
        return Optional.ofNullable(page)
                .map(pageItem -> pageItem.getAnnotation(DefaultUrl.class))
                .map(annotationValue -> annotationValue.value())
                .orElse(null);
    }

    public static void waitForAsyncExecution() {
        scriptToCheck = new StringJoiner(System.lineSeparator())
                .add("return ((typeof jQuery !== 'undefined') && (jQuery.active == 0))")
                .toString();
        final WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(e -> isPageLoaded());
    }

    private static Boolean isPageLoaded() {
        final Object scriptResult = ((JavascriptExecutor) getDriver())
                .executeScript(scriptToCheck);
        return Boolean.parseBoolean(String.valueOf(scriptResult));
    }
}
