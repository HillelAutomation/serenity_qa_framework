package core.common.utils;

import org.jbehave.core.model.ExamplesTable;
import org.jbehave.core.steps.Row;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JBehaveUtils {

    private JBehaveUtils() {
    }

    public static List<Map<String, String>> convert(final ExamplesTable examplesTable) {
        return examplesTable.getRowsAsParameters(true)
                .stream()
                .map(Row::values)
                .collect(Collectors.toList());
    }
}
