package core.common.utils;

import core.common.EnvironmentProperty;

import java.io.*;
import java.util.Properties;

public class PropertyUtils {

    private static final String PARTIAL_PROPERTIES_PATH = "src/main/resources/qa.properties";
    private Properties properties = new Properties();

    public PropertyUtils() {
        readProperties();
    }

    private void readProperties() {
        try (final InputStream inputStream = new FileInputStream(new File(PARTIAL_PROPERTIES_PATH))) {
            properties.load(inputStream);
        } catch (final IOException e) {
            throw new IllegalStateException("Something has come up!", e);
        }
    }

    public String getProperty(final String propertyKey) {
        return properties.getProperty(propertyKey);
    }
}


