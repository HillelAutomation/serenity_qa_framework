package core.common.panels;

import core.common.pages.AbstractPage;
import core.common.utils.WebDriverUtil;
import net.serenitybdd.core.pages.WebElementFacade;

public class ProductOptionsPanel extends AbstractPanel {

    private static final String SIZE_SELECT_LOCATOR = ".//select[@data-control-type='size']";
    private static final String QTY_SELECT_LOCATOR = ".//select[@name='quantity']";
    private static final String ADD_TO_CART_BUTTON_LOCATOR = ".//button[contains(@class, 'add-cart')]";

    public ProductOptionsPanel(final WebElementFacade panelBaseLocation, final AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
    }

    public void selectSize(final String size) {
        final WebElementFacade sizeSelect = getDriverDelegate().findBy(SIZE_SELECT_LOCATOR);
        sizeSelect.selectByVisibleText(size);
        WebDriverUtil.waitForAsyncExecution();
    }

    public void selectQty(final String qty) {
        final WebElementFacade qtySelect = getDriverDelegate().findBy(QTY_SELECT_LOCATOR);
        qtySelect.selectByVisibleText(qty);
        WebDriverUtil.waitForAsyncExecution();
    }

    public void clickAddToCartButton() {
        final WebElementFacade addToCartButton = getDriverDelegate().findBy(ADD_TO_CART_BUTTON_LOCATOR);
        addToCartButton.then().click();
        WebDriverUtil.waitForAsyncExecution();
    }
}
