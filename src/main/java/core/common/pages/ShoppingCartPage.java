package core.common.pages;

import core.common.panels.ProductPanel;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class ShoppingCartPage extends AbstractPage {

    private static final String PRODUCT_PANEL_LOCATOR = "//section[@class='sc-product']";

    public ShoppingCartPage(final WebDriver driver) {
        super(driver);
    }

    public ProductPanel getProductPanel() {
        return new ProductPanel(withTimeoutOf(10, TimeUnit.SECONDS).find(PRODUCT_PANEL_LOCATOR), this);
    }
}
