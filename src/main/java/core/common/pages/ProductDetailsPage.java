package core.common.pages;

import core.common.panels.HeaderPanel;
import core.common.panels.ProductOptionsPanel;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class ProductDetailsPage extends AbstractPage {

    private static final String PAGE_HEADER_LOCATOR = "//header[@class='page-header']";
    private static final String PRODUCT_OPTIONS_PANEL = "//div[@class='product-detail__options']";

    public ProductDetailsPage(final WebDriver driver) {
        super(driver);
    }

    public HeaderPanel getHeaderPanel() {
        final WebElementFacade headerPanel = withTimeoutOf(20, TimeUnit.SECONDS)
                .find(PAGE_HEADER_LOCATOR);
        return new HeaderPanel(headerPanel, this);
    }

    public ProductOptionsPanel getProductOptionsPanel() {
        final WebElementFacade productOptionsPanel = withTimeoutOf(20, TimeUnit.SECONDS)
                .find(PRODUCT_OPTIONS_PANEL);
        return new ProductOptionsPanel(productOptionsPanel, this);
    }
}
