package core.common.beans;

import lombok.Data;

public class ProductBuilder {

    private String title;
    private String qty;
    private String size;
    private String price;
    private String total;

    public ProductBuilder setTitle(final String title) {
        this.title = title;
        return this;
    }

    public ProductBuilder setQty(final String qty) {
        this.qty = qty;
        return this;
    }

    public ProductBuilder setSize(final String size) {
        this.size = size;
        return this;
    }

    public ProductBuilder setPrice(final String price) {
        this.price = price;
        return this;
    }

    public ProductBuilder setTotal(final String total) {
        this.total = total;
        return this;
    }

    public static ProductBuilder newInstance() {
        return new ProductBuilder();
    }

    public Product build() {
        return new Product(title, qty, size, price, total);
    }

    @Data
    public class Product {

        private String title;
        private String qty;
        private String size;
        private String price;
        private String total;

        Product(final String title, final String qty, final String size, final String price, final String total){
            this.title = title;
            this.qty = qty;
            this.size = size;
            this.price = price;
            this.total = total;
        }
    }
}

