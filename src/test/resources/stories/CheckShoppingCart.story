Meta:
@ShoppingCart

Narrative:
In order to buy a product
As a user
I want to be able to see sellable product addeed to Shopping Cart Page

Scenario: Check add product to Shopping Cart
Meta:
@ShoppingCart

Given user has opened product details page: /categories/men/footwear/training-shoes/product/adidas-mens-athletics-247-training-shoes-dark-greywhite-332282849.html
When user fill in following parameters:
| size | qty |
| size | qty |
And clicks 'ADD TO CART' button
And navigates to Shopping Cart Page
Then following product should be added to Shopping Cart:
| title                                                        | qty | price | total | size |
| adidas Men's Athletics 24/7 Training Shoes - Dark Grey/White | qty | price | total | size |

Examples:
| size | qty | total   | price  |
| 8    | 1   | $83.96  | $83.96 |
| 10   | 3   | $251.88 | $83.96 |
| 13   | 5   | $419.80 | $83.96 |