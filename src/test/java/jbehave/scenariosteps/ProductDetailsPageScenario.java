package jbehave.scenariosteps;

import core.common.utils.JBehaveUtils;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.jbehave.core.steps.Parameters;
import serenity.steps.HomePageSteps;
import serenity.steps.ProductDetailsPageSteps;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProductDetailsPageScenario {

    @Steps
    private HomePageSteps homePageSteps;

    @Steps
    private ProductDetailsPageSteps productDetailsPageSteps;

    @Given("user has opened product details page: $partialLink")
    public void navigateToProductDetailsPage(final String partialUrl) {
        homePageSteps.openProductDetailsPage(partialUrl);
    }

    @When("user fill in following parameters: $productParameters")
    public void fillInProductParameters(final ExamplesTable parameters) {
        final List<Map<String, String>> params = JBehaveUtils.convert(parameters);
        final String url = Serenity.sessionVariableCalled("base_url_key");
        params.forEach(item -> {
            productDetailsPageSteps.setSize(item.get("size"));
            productDetailsPageSteps.setQty(item.get("qty"));
        });
    }

    @When("clicks 'ADD TO CART' button")
    public void clickAddToCartButton() {
        productDetailsPageSteps.clickAddToCartButton();
    }
}
