package jbehave.scenariosteps;

import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.When;
import serenity.steps.ProductDetailsPageSteps;

public class CommonPagesScenario {

    @Steps
    private ProductDetailsPageSteps productDetailsPageSteps;

    @When("navigates to Shopping Cart Page")
    public void navigateToShoppingCart() {
        productDetailsPageSteps.navigateToShoppingCart();
    }
}
