package jbehave.scenariosteps;

import core.common.beans.ProductBuilder;
import core.common.utils.JBehaveUtils;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.model.ExamplesTable;
import org.unitils.reflectionassert.ReflectionAssert;
import serenity.steps.ShoppingCartPageSteps;

import java.util.List;
import java.util.Map;

public class ShoppingCartPageScenario {

    @Steps
    private ShoppingCartPageSteps shoppingCartPageSteps;

    @Then("following product should be added to Shopping Cart: $shoppingCartItemParameters")
    public void checkShoppingCartItems(final ExamplesTable shoppingCartItemParameters) {
        final List<Map<String, String>> productDetails = JBehaveUtils.convert(shoppingCartItemParameters);
        final ProductBuilder.Product actualProduct = shoppingCartPageSteps.getProductDetails();
        productDetails.forEach(product -> {
            final ProductBuilder.Product expectedProduct = ProductBuilder.newInstance()
                    .setPrice(product.get("price"))
                    .setQty(product.get("qty"))
                    .setSize(product.get("size"))
                    .setTitle(product.get("title"))
                    .setTotal(product.get("total"))
                    .build();
            ReflectionAssert.assertReflectionEquals("There is incorrect data in 'Product' present!", expectedProduct, actualProduct);
        });
    }
}
