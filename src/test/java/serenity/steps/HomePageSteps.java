package serenity.steps;

import core.common.EnvironmentProperty;
import core.common.pages.HomePage;
import core.common.utils.WebDriverUtil;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.sessions.TestSessionVariables;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class HomePageSteps extends ScenarioSteps {

    private HomePage homePage;

    public HomePageSteps(final Pages pages) {
        homePage = pages.getPage(HomePage.class);
    }

    @Step
    public void openProductDetailsPage(final String partialPath) {
        //homePage.openPageByExtraPath(HomePage.class, partialPath); // this method opens site by annotation
        final String fullPath = EnvironmentProperty.BASE_SITE_URL.readProperty() + partialPath;// => https://www.stg.sportchek.ca//categories/men/footwear/training-shoes/product/adidas-mens-athletics-247-training-shoes-dark-greywhite-332282849.html
        homePage.openByFullPath(fullPath);
        Serenity.setSessionVariable("base_url_key").to(partialPath);
        WebDriverUtil.waitForAsyncExecution();
    }
}
