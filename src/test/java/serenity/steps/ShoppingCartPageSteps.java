package serenity.steps;

import core.common.beans.ProductBuilder;
import core.common.pages.ShoppingCartPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;
import org.apache.commons.lang3.StringUtils;

public class ShoppingCartPageSteps extends ScenarioSteps {

    private ShoppingCartPage shoppingCartPage;

    public ShoppingCartPageSteps(final Pages pages) {
        super(pages);
    }

    @Step
    public ProductBuilder.Product getProductDetails() {
        final ProductBuilder.Product product = ProductBuilder.newInstance()
                .setPrice(getPrice())
                .setQty(getQty())
                .setSize(getSize())
                .setTitle(getTitle())
                .setTotal(getTotal())
                .build();
        return product;
    }

    @Step
    public String getTitle() {
        return shoppingCartPage.getProductPanel().getTitle();
    }

    @Step
    public String getQty() {
        return shoppingCartPage.getProductPanel().getQty();
    }

    @Step
    public String getPrice() {
        return shoppingCartPage.getProductPanel().getPrice();
    }

    @Step
    public String getTotal() {
        return shoppingCartPage.getProductPanel().getTotal();
    }

    @Step
    public String getSize() {
        final String size = shoppingCartPage.getProductPanel().getSize();
        return StringUtils.getDigits(size);
    }
}
